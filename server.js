const express = require('express');
const axios = require('axios');
const app = express();
const { stringParser,
  removeDup,
  insertAdsIntoStory,
  dateParser,
  adTagUrl,
  convertEmbed,
  convertFacebookEmbed,
  convertInstagramEmbed,
  convertImageEmbed,
  convertEncodedUrl,
  insertMediaIntoCarousel } = require('./utils');


app.set('views', 'src');
app.set('view engine', 'pug');
app.use(express.static(__dirname + '/src'));
const baseUrl = 'http://production-ro-platform-cached.sinclairstoryline.com/api/rest/facade/page';
app.get('*', (req, res) => {
  const { hostname, query } = req;
  const slug =  query.slug ? query.slug : 'demonews3';
  const moreStories =`https://${slug}.com/api/rest/audience/more?section=${slug}.com`;
  const storyUrl = `https://${slug}.com/api/rest/facade/story`;
  const storyPath = req.url;

  axios.get(`${baseUrl}?siteSlug=${slug}&path=${storyPath}`).then(data => {
    const pageData = data.data.data[0];
    const { rendering, content } = pageData;
    if(rendering) {
      const { canonicalUrl, pageTitle, sectionUrl, site, adZone, dfpAdUnitId, dfpNetworkId } = rendering;
      const adRoot ='Mobile';
      const ads = {dfpNetworkId,dfpAdUnitId,adRoot,adZone};
      const adStoryUrl = `https://${slug}.com${canonicalUrl}`;
      const videoAdsUrl = adTagUrl(ads, 'preroll', 'VOD', adStoryUrl);
      const { main_content }  = content;
      const { headerLogoImageUrl, brandName, social, brandColorOne, brandColorTwo } = site;
      const { facebookAppId }  = social;
      const storyData = main_content.story;
      const { headline, publishedDateISO8601, byLine, heroImage, richText, images, videos } = storyData;

      let storyBodyText = insertAdsIntoStory(stringParser(richText));
      while (storyBodyText.indexOf("<sd-embed") > -1) {
        storyBodyText = convertEmbed(storyBodyText, slug);
      }

      const storyPublishedDate = dateParser(publishedDateISO8601);
      const ampCarouselComponent = insertMediaIntoCarousel(heroImage, images, videos, videoAdsUrl, slug);

      axios.get(`${moreStories}${sectionUrl}&limit=4&offset=0`).then(more => {
        const moreStoriesList = removeDup(more.data.data, storyData);
        const pages = [];
        const slugUrl = query.slug ? `?slug=${slug}` : '';
        const hideSelectors = [".page-header", ".page-footer"];
        moreStoriesList.map(({ url, title, heroImageUrl }) => {
          pages.push({ ampUrl: `${url}${slugUrl}`, image: heroImageUrl ? `//${slug}.com${heroImageUrl}` : '', title });
        });
        const nextPageData = JSON.stringify({pages, hideSelectors});
        const hasVideo = videos.length > 0;
        const headerLogoUrl = `https://${slug}.com${headerLogoImageUrl}`;

        res.render('index', {
            pageTitle,
            canonicalUrl,
            headline,
            byLine,
            publishedDateISO8601,
            nextPageData,
            storyBodyText,
            hasVideo,
            storyPublishedDate,
            headerLogoUrl,
            brandName,
            brandColorOne,
            brandColorTwo,
            facebookAppId,
            ampCarouselComponent,
        });
      })
    }

  }).catch((err) => {
    console.log('errors', err)
  })
});

const server = app.listen(7000, () => {
  console.log(`Server running → PORT ${server.address().port}`);
});
