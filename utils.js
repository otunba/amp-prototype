const stringParser = (richText) => {
    const re = /{((?:[^{}])+)+}/gi;
    // Remove the blank space tags and replace curly tags.
    return richText.replace(/{&nbsp;}/gi, ' ').replace(re, ['<$1>']);
}


const removeDup = (list, firstStory) => {
 const uniq = list.filter((item, i) => { return item.storyUuid !== firstStory.uuid });
 return uniq;
}


const insertAdsIntoStory = (storyText) => {
  let modifiedStoryText = "";
  const ampAd300x250 = `<amp-ad class="in-story-block-ad" type="doubleclick" width="300" height="250" data-slot="/4756/wjla/Mobile"></amp-ad>`;
  const firstPLocation = storyText.indexOf("<p");
  const secondPLocation = storyText.indexOf("<p", firstPLocation + 1);
  const thirdPLocation = storyText.indexOf("<p", secondPLocation + 1);
  const numberOfParagraphsInStory = countPs(storyText);

  if (secondPLocation > -1) {
    modifiedStoryText = storyText.substring(0, secondPLocation);
    modifiedStoryText += ampAd300x250;
    modifiedStoryText += storyText.substring(secondPLocation);

    if (thirdPLocation > -1) {
      const lastPLocation = modifiedStoryText.lastIndexOf("<p");
      let originalModifiedStoryText = modifiedStoryText;
      modifiedStoryText = originalModifiedStoryText.substring(0, lastPLocation);
      modifiedStoryText += ampAd300x250;
      modifiedStoryText += originalModifiedStoryText.substring(lastPLocation);

      if (numberOfParagraphsInStory > 7) {
        const paragraphToInsertAfter = Math.floor(numberOfParagraphsInStory / 2);
        let insertionPoint = -1;
        for (let i = 0; i <= paragraphToInsertAfter; i++) {
            insertionPoint = modifiedStoryText.indexOf("<p", insertionPoint + 1);
        }

        const storyTextPreAd = modifiedStoryText.substring(0, insertionPoint);
        const storyTextPostAd = modifiedStoryText.substring(insertionPoint);
        modifiedStoryText = `${storyTextPreAd}${ampAd300x250}${storyTextPostAd}`;
      }
    }
  }

  return modifiedStoryText;
}


const countPs = (htmlText) => {
    let paragraphsFound = 0;
    let mostRecentParagraphPosition = -1;
    while (htmlText.indexOf("<p", mostRecentParagraphPosition + 1) > -1) {
        paragraphsFound++;
        mostRecentParagraphPosition = htmlText.indexOf("<p", mostRecentParagraphPosition + 1);
    }

    return paragraphsFound;
}


const dateParser = (storyDate) => {
    const rawDate = new Date(storyDate);
    const dayIndex = rawDate.getDay();
    const dateIndex = rawDate.getDate();
    const monthIndex = rawDate.getMonth();
    const ordinal = (dateIndex === 1 || dateIndex === 21 || dateIndex === 31)
        ? "st"
        : (dateIndex === 2 || dateIndex === 22)
            ? "nd"
            : (dateIndex === 3 || dateIndex === 23)
                ? "rd"
                : "th";
    const fullYear = rawDate.getFullYear();
    const dayName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dayIndex];
    const monthName = ["January","February","March","April","May","June","July","August","September","October","November","December"][monthIndex];

    return `${dayName}, ${monthName} ${dateIndex}${ordinal} ${fullYear}`;
}


const convertEmbed = (storyText, slug) => {
  const embedLocation = storyText.indexOf("<sd-embed");
  if (embedLocation === -1) { return storyText }

  const firstCharacterAfterEmbed = storyText.indexOf("</sd-embed>") + 11;
  const preEmbedStoryText = storyText.substring(0, embedLocation);
  const postEmbedStoryText = storyText.substring(firstCharacterAfterEmbed);
  const embedHtmlString = storyText.substring(embedLocation, firstCharacterAfterEmbed);

  // Detect what kind of embed we have
  const embedTypeLocation = embedHtmlString.indexOf("data-embed-type=");
  const embedTypeLocationEnd = embedHtmlString.indexOf('"', embedTypeLocation + 17);
  const detectedEmbedType = embedHtmlString.substring(embedTypeLocation + 17, embedTypeLocationEnd);
  // console.log("Detected an embed of type: " + detectedEmbedType);
  switch (detectedEmbedType) {
    case "facebook":
      const ampComponentFB = convertFacebookEmbed(embedHtmlString);
      return `${preEmbedStoryText}${ampComponentFB}${postEmbedStoryText}`;
      break;
    case "image":
      const ampComponentImage = convertImageEmbed(embedHtmlString, slug);
      return `${preEmbedStoryText}${ampComponentImage}${postEmbedStoryText}`;
      break;
    case "instagram":
      const ampComponentInsta = convertInstagramEmbed(embedHtmlString);
      return `${preEmbedStoryText}${ampComponentInsta}${postEmbedStoryText}`;
      break;
    default:
      return `${preEmbedStoryText}${postEmbedStoryText}`;
  }
}


const convertFacebookEmbed = (embedHtmlString) => {
  // Determine if it is a regular FB embed or a FB video embed
  const videoEmbedIndicator = embedHtmlString.indexOf("video.php");
  if (videoEmbedIndicator > -1) {
    // Video embed
    const ampComponentPrefix = "<amp-facebook width='476' height='316' layout='responsive' data-embed-as='video' data-href='";
    const ampComponentSuffix = "'></amp-facebook>";
    const videoUrlLocationStart = videoEmbedIndicator + 15;
    const videoUrlLocationEnd = embedHtmlString.indexOf("&", videoEmbedIndicator);
    const fbVideoUrl = embedHtmlString.substring(videoUrlLocationStart, videoUrlLocationEnd);
    // console.log("Original FB video URL: " + fbVideoUrl);
    const decodedFbUrl = convertEncodedUrl(fbVideoUrl);
    return `${ampComponentPrefix}${decodedFbUrl}${ampComponentSuffix}`;
  } else {
    // Non-video embed
    return "";
  }
  // return string which is an amp component
}


const convertInstagramEmbed = (embedHtmlString) => {
  const instaCodeList = embedHtmlString.split('https://www.instagram.com')[1].split('/p/');
  const instaCode = instaCodeList[1].split('/');
  const ampComponent = `
  <amp-instagram
    data-shortcode=${instaCode[0]}
    data-captioned
    width="400"
    height="400"
    layout="responsive">
  </amp-instagram>
  `
  return ampComponent;
}


const convertImageEmbed = (embedHtmlString, slug) => {
  const ampComponentPrefix = "<figure><amp-img width='160' height='90' layout='responsive' alt='image embed' src='";
  const ampComponentSuffix = "'></amp-img><figcaption class='image-embed-caption'>";
  const ampComponentSuffix2 = "</figcaption></figure>";

  const mediumImageLocator = embedHtmlString.indexOf("%22medium16x9%22:");
  const mediumUrlLocation = embedHtmlString.indexOf("%22url%22:", mediumImageLocator);
  const mediumUrlRealLocation = mediumUrlLocation + 13;
  const mediumUrlFirstCharacterToExclude = embedHtmlString.indexOf("%22,", mediumUrlRealLocation);
  const thisIsTheUrlYouAreLookingFor = embedHtmlString.substring(mediumUrlRealLocation, mediumUrlFirstCharacterToExclude);

  const captionStart = embedHtmlString.indexOf("data-caption=") + 14;
  const captionEnd = embedHtmlString.indexOf('"', captionStart);
  const captionText = embedHtmlString.substring(captionStart, captionEnd);
  const decodedCaptionText = decodeURIComponent(captionText);

  return `${ampComponentPrefix}https://${slug}.com${thisIsTheUrlYouAreLookingFor}${ampComponentSuffix}${decodedCaptionText}${ampComponentSuffix2}`;
}


const convertEncodedUrl = (encodedString) => {
  let decodedString = encodedString;

  while (decodedString.indexOf("%253A") > -1) {
    decodedString = decodedString.replace("%253A", ":");
  }
  while (decodedString.indexOf("%252F") > -1) {
    decodedString = decodedString.replace("%252F", "/");
  }

  return decodedString.substring(0, decodedString.length - 1);
}


const insertMediaIntoCarousel = (heroImage, images, videos, adsUrl, slug) => {
    //let carouselComponent = `<amp-carousel id="carousel-with-lightbox" width="400" height="300" layout="responsive" type="slides" lightbox>`;
    let carouselComponent = `<amp-carousel width="160" height="105" layout="responsive" type="slides">`;

    //add video if any
    if (videos.length > 0) {
        const videoThumbUrl = videos[0].thumbUrl;
        const videoUrl = (videos[0].mp4Url).replace("http","https");
        const videoTitle = videos[0].title;
        const carouselVideo = `
        <div class="heroCarouselSlide">
        <amp-ima-video
            width=640
            height=360
            layout="responsive"
            data-src="${videoUrl}"
            data-tag=${adsUrl}
            data-poster=${videoThumbUrl}>
          <script type="application/json">
          {
            "locale": "en",
            "numRedirects": 4
          }
          </script>
        </amp-ima-video>
        <div class="heroCarouselCaption">${videoTitle}</div></div>`;
        carouselComponent += carouselVideo;
    }

    //add hero image
    if (heroImage != null) {
        const heroImageUrl = `https://${slug}.com${heroImage.image.medium16x9Url}`;
        const heroImageCaption = heroImage.image.caption;
        const carouselImage = `<div class="heroCarouselSlide"><amp-img width="160" height="90" src="${heroImageUrl}" layout="responsive"></amp-img><div class="heroCarouselCaption">${heroImageCaption}</div></div>`;
        carouselComponent += carouselImage;
    }

    //add other images
    if (images.length != 0) {
        //sort images by order
        let sortedImages = {};
        images.forEach(function (image) {
            sortedImages[image.orderNumber] = image;
        });
        for (var key in sortedImages) {
            //hero image already added so do not duplicate
            if (key != heroImage.image.orderNumber) {
                let carouselImageUrl = `https://demonews3.com${sortedImages[key].medium16x9Url}`;
                let carouselImageCaption = sortedImages[key].caption;
                let carouselImage = `<div class="heroCarouselSlide"><amp-img src="${carouselImageUrl}" width="160" height="90" layout="responsive"></amp-img><div class="heroCarouselCaption">${carouselImageCaption}</div></div>`;
                carouselComponent += carouselImage;
            }
        }
    }

    if (heroImage == null && videos == null && images == null) {
        return "";
    }

    carouselComponent += `</amp-carousel>`;
    return carouselComponent;
}

const createUrlParams = (payload) =>  {
 const urlParams = Object.keys(payload).map((k) => {
 return encodeURIComponent(k) + '=' + encodeURIComponent(payload[k])
 }).join('&');
 return urlParams;
}

const adTagUrl = ({
    dfpNetworkId,
    dfpAdUnitId,
    adRoot,
    adZone,
}, schedule, videoType, url) =>  {
    const correlator = Math.floor(Math.random() * 9876543210987);
    const adsPayload = {
        sz: '640x480',
        iu: [dfpNetworkId,dfpAdUnitId,adRoot,adZone].join('/'),
        ciu_szs: '300x250',
        impl: 's',
        gdfp_req: 1,
        env: 'vp',
        vpos: schedule,
        output: 'xml_vast3',
        unviewed_position_start: 1,
        url,
        description_url: url,
        correlator,
        cust_params: `kw=""&slug=${url}&vtype=${videoType}`,
    };
    const params = createUrlParams(adsPayload);
    return '//pubads.g.doubleclick.net/gampad/ads?' + params.replace('&=', '');
}

module.exports = {
    stringParser,
    removeDup,
    insertAdsIntoStory,
    dateParser,
    adTagUrl,
    convertEmbed,
    convertFacebookEmbed,
    convertInstagramEmbed,
    convertImageEmbed,
    convertEncodedUrl,
    insertMediaIntoCarousel,
};
